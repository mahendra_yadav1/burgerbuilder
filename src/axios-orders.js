import axios from 'axios';

const instance = axios.create({
    baseURL:'https://react-my-burgerbuilder-5e1fc.firebaseio.com/'
});

export default instance;