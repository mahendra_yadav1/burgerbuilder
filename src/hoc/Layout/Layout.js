import React, {useState} from 'react';
import Aux from '../_Aux/_Aux';
import classes from './Layout.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';
import {connect} from 'react-redux'
const layout = props => {

    const [showSideDrawer,setshowSideDrawer] = useState(false);

    const sideDrawerClosedHandler = () => {
        setshowSideDrawer(false);
    }

    const sideDrawerToggleHandler = () => {
       setshowSideDrawer(!showSideDrawer);
    }

    
        return(
            <Aux>
                <Toolbar 
                    isAuth = {props.isAuthenticated}
                    drawerToggleClicked={sideDrawerToggleHandler}/>
                <SideDrawer 
                    isAuth = {props.isAuthenticated}
                    open = {showSideDrawer}
                    closed = {sideDrawerClosedHandler} />
                <main className={classes.Content}>
                    {props.children}
                </main>
            </Aux>
        );
    
}
   
const mapStateToProps = state => {
    return {
        isAuthenticated : state.auth.token !=null
        }
}

export default connect(mapStateToProps)(layout);